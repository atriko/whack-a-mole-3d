﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private TextMeshProUGUI scoreText;
    private static int totalScore;

    public static int GetScore()
    {
        return totalScore;
    }
    void Awake()
    {
        scoreText = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<TextMeshProUGUI>();
        totalScore = 0;
        Enemy.pointsScored += AddPoints;
    }

    private void AddPoints(int points)
    {
        totalScore += points;
        AudioManager.Instance.Play("GotPoints");
        scoreText.text = "Score : " + totalScore.ToString();    
    }
    private void OnDestroy()
    {
        Enemy.pointsScored -= AddPoints;
    }
}
