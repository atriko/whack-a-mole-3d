﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RedGoblin : Enemy
{
    [SerializeField]
    private Image healthbar = default;

    private void Start()
    {
        maxHealth = 5f; // Red Goblin Health
        currentHealth = maxHealth;
        aliveTime = 2.5f; // Red Goblin Alive Time
        score = 100; // Red Goblin worth points
        
        StartCoroutine(AliveTimer()); // Start the alive timer
        myAnimator = GetComponent<Animator>();
    }
    public override void UpdateHealthbar()
    {
        base.UpdateHealthbar();
        healthbar.fillAmount = currentHealth / maxHealth;
    }
}
