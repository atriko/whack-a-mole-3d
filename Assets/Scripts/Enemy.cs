﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enemy : MonoBehaviour
{
    public static event Action<int> pointsScored = delegate { }; // Event for score manager
    public static event Action<GameObject> enemyDead = delegate { }; // Event for game manager

    public Animator myAnimator;
    public GameObject spawnPoint;

    public float maxHealth;
    public float currentHealth;
    public float aliveTime;
    public int score;

    private void OnMouseDown()
    {
        GetHit();
    }

    private void OnMouseUp()
    {
        ResetHitStates();
    }

    public void GetHit()
    {
        if (CreateHammer()) // Testing if you can hit, prevents spamming mouse click
        {
            AnimateHit();
            LoseHealth();
            AudioManager.Instance.Play("Hurt");
        }
    }

    private bool CreateHammer() //
    {
        if (!GameObject.Find("Hammer")) // If there is a hammer already you cant create another
        {
            Vector3 hammerPos = Input.mousePosition;
            hammerPos.z = spawnPoint.transform.position.z;
            GameObject hammer = Instantiate(GameManager.Instance.hammerPrefab, Camera.main.ScreenToWorldPoint(hammerPos), GameManager.Instance.hammerPrefab.transform.rotation);
            hammer.name = "Hammer";
            Destroy(hammer, 0.33f);
            CreateSparks();
            return true;
        }
        else
        {
            return false;
        }
        
    }
    private void CreateSparks() 
    {
        // Setting spark position from mouse and it's depth from spawn point
        Vector3 sparkPosition = Input.mousePosition; 
        sparkPosition.z = spawnPoint.transform.position.z;
        GameObject spark = Instantiate(GameManager.Instance.particlePrefab, Camera.main.ScreenToWorldPoint(sparkPosition), Quaternion.identity);
        Destroy(spark, 0.33f); // Destroy spark after third of a second

    }

    public void LoseHealth()
    {
        currentHealth--;
        if (currentHealth == 0)
        {
            pointsScored(score);
            Die();
        }
        else
        {
            UpdateHealthbar();
        }
    }

    public void Die()
    {
        enemyDead(spawnPoint); // Calling the event to inform game manager
        if (gameObject != null)
        {
            Destroy(gameObject);
        }
    }

    public void ResetHitStates()
    {
        myAnimator.SetBool("hit1", false);
        myAnimator.SetBool("hit2", false);
    }

    public void AnimateHit() // Choosing from 2 different animations
    {
        int randomizeAnimation = UnityEngine.Random.Range(0, 2);
        if (randomizeAnimation == 0)
        {
            myAnimator.SetBool("hit1", true);

        }
        else
        {
            myAnimator.SetBool("hit2", true);
        }
    }

    public IEnumerator AliveTimer()
    {
        yield return new WaitForSeconds(aliveTime);
        AudioManager.Instance.Play("LostGoblin");
        Die();
    }
    
    public virtual void UpdateHealthbar() { } // Virtual method for healthbar, child classes will modify it with their own healthbar
    
}
