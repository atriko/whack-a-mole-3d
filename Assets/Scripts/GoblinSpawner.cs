﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GoblinSpawner : MonoBehaviour
{
    public static event Action<GameObject> Spawned = delegate { };

    [SerializeField]
    private GameObject greenGoblinPrefab = default;
    [SerializeField]
    private GameObject redGoblinPrefab = default;

    private float spawnRate = 1f; // Spawn goblins every x seconds
    private float nextSpawn = 0f; // Spawn timer counter starts from zero

    private GameObject[] spawnChoices; // Available locations
    private GameObject spawnLocation; // Chosen location to spawn
    private Transform lookPoint;
    private bool isGameOver;

    private void Start()
    {
        spawnChoices = GameManager.Instance.GetSpawnPoints().ToArray(); // Get the choices
        lookPoint = GameObject.FindWithTag("Player").transform;
        Timer.TimesUp += StopSpawner;
        isGameOver = false;
    }

    private void StopSpawner()
    {
        isGameOver = true;

    }

    void Update()
    {
        if (CanSpawn() && HaveSpawnLocations())
        {
            SpawnGoblin(ChooseGoblin());
            AudioManager.Instance.Play("Spawn");
        }
        
    }

    private GameObject ChooseGoblin()
    {
        float random = UnityEngine.Random.Range(1f, 10f);
        if (random < 2f) // %10 chance to get the red goblin
        {
            return redGoblinPrefab;
        }
        else
        {
            return greenGoblinPrefab;
        }
    }

    private bool HaveSpawnLocations()
    {
        return spawnChoices.Length > 0 ? true : false; // If there is no available place to spawn returns false
    }

    private void SpawnGoblin(GameObject goblinPrefab)
    {
            spawnChoices = GameManager.Instance.GetSpawnPoints().ToArray(); // Get the choices
            PickSpawnLocation();
            GameObject newGoblin = Instantiate(goblinPrefab, spawnLocation.transform.position, goblinPrefab.transform.rotation);
            newGoblin.name = "Goblin";
            newGoblin.transform.LookAt(lookPoint); // Goblins are turning to camera
            newGoblin.transform.parent = spawnLocation.transform;
            newGoblin.GetComponent<Enemy>().spawnPoint = spawnLocation;
            Spawned(spawnLocation); // Calling the event for informing game manager
    }

    private void PickSpawnLocation() // Choose a random spawn point
    {
        int rand = UnityEngine.Random.Range(0, spawnChoices.Length);
        spawnLocation = spawnChoices[rand];
    }

    private bool CanSpawn()
    {
        if (Time.time > nextSpawn && !isGameOver)
        {
            nextSpawn = spawnRate + Time.time;
            return true;
        }
        return false;
    }
}
