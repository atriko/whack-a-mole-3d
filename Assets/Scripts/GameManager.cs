﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Singleton
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }
    #endregion

    private List<GameObject> spawnPoints = new List<GameObject>();
    
    public GameObject particlePrefab = default; // Particle prefab given from the inspector
    public GameObject hammerPrefab = default; // Particle prefab given from the inspector

    private float timeLimit = 60f; // Time limit for the game
    public float GetTimeLimit { get => timeLimit; private set { } }
    
    private void Start()
    {
        //Eventhandlers
        GoblinSpawner.Spawned += RemoveSpawnPoint;
        Enemy.enemyDead += AddSpawnPoint;
        Timer.TimesUp += EndGame;
    }
    private void Awake()
    {
        if (instance != this && instance != null)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        //Get all spawn points
        spawnPoints.AddRange(GameObject.FindGameObjectsWithTag("SpawnPoint"));
    }
    private void EndGame()
    {
        SceneManager.LoadScene(2);
    }

    private void AddSpawnPoint(GameObject spawnPoint) // Called when a goblin dead or escaped
    {
        spawnPoints.Add(spawnPoint);
    }

    private void RemoveSpawnPoint(GameObject spawnPoint) // Called when a goblin spawned
    {
        spawnPoints.Remove(spawnPoint);
    }
    public List<GameObject> GetSpawnPoints()
    {
        return spawnPoints;
    }
    
}
