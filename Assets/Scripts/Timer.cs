﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public static event Action TimesUp = delegate { };

    private TextMeshProUGUI timerText;
    private float timeRemaining;

    // Start is called before the first frame update
    void Start()
    {
        timeRemaining = GameManager.Instance.GetTimeLimit;
        timerText = GetComponent<TextMeshProUGUI>();
    }
    private void Update()
    {
        UpdateTime();
    }

    private void UpdateTime()
    {
        timeRemaining -= Time.deltaTime;
        if (timeRemaining > 0)
        {
            float minutes = Mathf.Floor(timeRemaining / 60);
            float seconds = Mathf.Floor(timeRemaining % 60);
            timerText.text = "Time : " + minutes.ToString("00") + ":" + seconds.ToString("00");
        }
        else
        {
            TimesUp();
        }
    }
}
