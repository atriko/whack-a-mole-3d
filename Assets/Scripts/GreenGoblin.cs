﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GreenGoblin : Enemy
{
    [SerializeField]
    private Image healthbar = default;

    private void Start()
    {
        maxHealth = 2f; // Goblin Health
        aliveTime = 4f; // Green Goblin Alive Time
        score = 10; // Green Goblin worth points
        currentHealth = maxHealth;
        myAnimator = GetComponent<Animator>();
        StartCoroutine(AliveTimer()); // Start the alive timer
    }

    public override void UpdateHealthbar()
    {
        base.UpdateHealthbar();
        healthbar.fillAmount = currentHealth / maxHealth;
    }
}
