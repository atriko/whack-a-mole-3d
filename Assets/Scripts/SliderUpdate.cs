﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderUpdate : MonoBehaviour
{
    Slider mySlider;
    // Start is called before the first frame update
    void Start()
    {
        mySlider = GetComponent<Slider>();
    }

    // Update is called once per frame
    public void SliderChanged()
    {
        AudioManager.Instance.AdjustVolume("MainTheme",mySlider.value);
    }
}
